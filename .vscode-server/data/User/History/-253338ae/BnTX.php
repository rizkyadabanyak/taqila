<?php

class AdminJenis extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "jenis";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT * FROM AREA_MITRA";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		$data['detail'] = $hasil;
		
		$date_now = date('d M y');
		$date_sixMounthAgo = date('d M y',strtotime("-180 day",strtotime(date('d M y'))));
		$sqlnotifikasi = "SELECT KM.ID_KEGIATAN_MITRA, M.NAMA_MITRA, K.NAMA_KEGIATAN, KM.TIMESTAMP
		FROM KEGIATAN_MITRA KM
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KM.ID_MITRA
		FULL OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
		WHERE KM.TIMESTAMP NOT BETWEEN '$date_sixMounthAgo' AND '$date_now'";


		$notifikasi = oci_parse($con,$sqlnotifikasi);
		oci_execute($notifikasi);
		oci_fetch_all($notifikasi, $p, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		$data['notifikasi'] = $notifikasi;

	    $this->view('views/partials-admin/header1',$data);
	    $this->view('views/admin/master/index_area',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer1');

    }
}