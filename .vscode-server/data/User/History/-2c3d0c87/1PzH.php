<script>

var map = L.map('mapid').setView([23.249406512898343, 67.25277836856507], 1);

var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);

var markers = L.markerClusterGroup();

<?php
    
        oci_execute($data['detail']);
        while($rows=oci_fetch_array($data['detail'])){
            $nama = $rows['NAMA_MITRA'];
            $judul = $rows['JUDUL_KERJASAMA'];
            $jenis = $rows['NAMA_JENIS'];
            $alamat = $rows['ALAMAT_MITRA'];
            $pic = $rows['PIC_MITRA'];
            $status = $rows['NAMA_STATUS'];
            $tgl_berakhir = $rows['TGL_BERAKHIR'];
            $lat = $rows['LATITUDE'];
            $lng = $rows['LONGITUDE']
 
        if ($rows['NAMA_STATUS'] == "Active"){
            $src=?>'<?= base_url; ?>/assets/images/Group_28.svg'<?php
        }elseif($rows['NAMA_STATUS'] == "Passive"){
            $src=?>'<?= base_url; ?>/assets/images/Group_25.svg'<?php
        }elseif($rows['NAMA_STATUS'] == "2 month left"){
            $src=?>'<?= base_url; ?>/assets/images/Group_27.svg'<?php
        }
        ?>

        var marker = L.marker([<?=$lat?>, <?=$lng?>]).bindPopup(
            " <img src=<?=$src?> /> <br/> <br/><b>Detail Informasi <?=$nama?> </b> <br/> <br/> <b> Nama mitra :</b> <?=$nama?> <br/> <b>Judul mitra : </b> <?=$judul?> <br/> <b> Jenis kerjasama : </b> <?=$jenis?><br/> <b> Alamat mitra : </b><?=$alamat?> <br/><b> PIC mitra : </b><?=$pic?> <br/><b> Status : </b><?=$status?> hingga <?=$tgl_berakhir?>"
            );
        markers.addLayer(marker);
        <?php
        }
        ?>
        map.addLayer(markers);

</script>