<?php

class AdminFakturKerjasama extends Controller {
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "faktur";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT F.ID_FAKTUR, M.NAMA_MITRA, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, F.PIC_PENS, S.NAMA_STATUS, F.TGL_MITRA, F.TGL_BERAKHIR
		FROM FAKTUR F
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
		FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
		FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
		FULL OUTER JOIN STATUS_MITRA S ON  S.ID_STATUS = F.ID_STATUS";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['detail'] = $hasil;

	    $this->view('views/partials-admin/header1',$data);
	    $this->view('views/admin/fakturKerjasama/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer1');
	}
}