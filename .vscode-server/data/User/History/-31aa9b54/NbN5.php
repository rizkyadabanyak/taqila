

<!-- Javascripts -->

<script src="<?= base_url; ?>/assets/plugins/jquery/jquery-3.4.1.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/bootstrap/popper.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?= base_url; ?>/assets/js/pages/datatables.js"></script>
<script src="<?= base_url; ?>/assets/plugins/DataTables/datatables.min.js"></script>
<script src="<?= base_url; ?>/assets/js/connect.min.js"></script>
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<script src="assets\Leaflet.markercluster-master/dist/leaflet.markercluster-src.js"></script>
</body>
</html>