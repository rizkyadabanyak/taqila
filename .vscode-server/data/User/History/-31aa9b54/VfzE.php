</div>

<!-- Javascripts -->
<script src="<?= base_url; ?>/assets/plugins/jquery/jquery-3.4.1.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/bootstrap/popper.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url; ?>/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>
<script src="<?= base_url; ?>/assets/js/pages/datatables.js"></script>
<script src="<?= base_url; ?>/assets/plugins/DataTables/datatables.min.js"></script>
<script src="<?= base_url; ?>/assets/js/connect.min.js"></script>
<script src="<?= base_url; ?>/assets/js/pages/charts.js"></script>
</body>
</html>