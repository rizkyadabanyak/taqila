<?php

class AdminBentuk extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "bentuk";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$querySql = "SELECT * FROM TELE";

		$tele = oci_parse($con, $querySql);
		oci_execute($tele);

		oci_fetch_all($tele, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		$data['tele'] = $tele;
		
		$date_now = date('d M y');
		$date_sixMounthAgo = date('d M y',strtotime("-180 day",strtotime(date('d M y'))));
		$sqlnotifikasi = "SELECT KM.ID_KEGIATAN_MITRA, M.NAMA_MITRA, K.NAMA_KEGIATAN, KM.TIMESTAMP
		FROM KEGIATAN_MITRA KM
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KM.ID_MITRA
		FULL OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
		WHERE KM.TIMESTAMP NOT BETWEEN '$date_sixMounthAgo' AND '$date_now'";


		$notifikasi = oci_parse($con,$sqlnotifikasi);
		oci_execute($notifikasi);
		oci_fetch_all($notifikasi, $p, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		$data['notifikasi'] = $notifikasi;

	    $this->view('views/partials-admin/header1',$data);
	    $this->view('views/admin/master/index_tele',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer1');

    }
}