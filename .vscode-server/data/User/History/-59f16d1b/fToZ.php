
            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Detail Kegiatan</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds many advanced features to any HTML table.</p>
                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Zero Configuration</h5>
                                    <p>DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.</p>
                                    <div></div>
                                    <table id="zero-conf" class="display" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Nama Departemen</th>
                                                <th>Kepala</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                             oci_execute($data['detail']);
                                             while($rows=oci_fetch_object($data['detail'])){
                                            ?>
                                            <tr>
                                                <td><?php echo $rows->DEPARTEMEN;?></td>
                                                <td><?php echo $rows->KEPALA;?></td>
                                            </tr>
                                            <?php
                                             }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nama Departemen</th>
                                                <th>Kepala</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>