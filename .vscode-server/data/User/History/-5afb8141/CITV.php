<div class="page-sidebar">
            <div class="logo-box">
                <a href="#" id="sidebar-close">
                    <i class="material-icons">close</i>
                </a> 
            </div>
            <div class="page-sidebar-inner slimscroll">
                <ul class="accordion-menu">
                    <li class="sidebar-title">
                       Hi, <?= $_SESSION['name'] ?>
                    </li>
                    <li class="<?php if($data['halaman']=='admin'){echo 'active-page';}?> active-title">
                        <a href="<?= base_url; ?>/Admin" class="active">
                            <i class="material-icons">bar_chart</i>
                            Statistik IDUKA
                        </a>
                    </li>
                    <li class=" <?php if($data['halaman']=='kegiatan'){echo 'active-page';}?> active-title">
                        <a href="<?= base_url; ?>/AdminKegiatan" class="active">
                            <i class="material-icons-outlined">
                                work_outline
                            </i>
                            Kegiatan IDUKA
                        </a>
                    </li>
                    <li class="<?php if($data['halaman']=='prodi'){echo 'active-page';}?> active-title">
                        <a href="<?= base_url; ?>/AdminKerjasamaProdi" class="active">
                            <i class="material-icons-outlined">
                                business_center
                            </i>
                            Kerjasama Prodi 
                        </a>
                    </li>
                    <li>
                        <a href="#"><i class="material-icons">card_giftcard</i>Folder<i class="material-icons has-sub-menu">add</i></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="extended-select2.html">Select2</a>
                                </li>
                                <li>
                                    <a href="extended-datatables.html">Data Tables</a>
                                </li>
                                <li>
                                    <a href="extended-blockui.html">Block UI</a>
                                </li>
                                <li>
                                    <a href="extended-session.html">Session Timeout</a>
                                </li>
                                <li>
                                    <a href="extended-tree.html">Tree View</a>
                                </li>
                            </ul>
                    </li>
                    <li class="<?php if($data['halaman']=='faktur'){echo 'active-page';}?> active-title">
                        <a href="<?= base_url; ?>/AdminFakturKerjasama" class="active">
                            <i class="material-icons-outlined">
                            description
                            </i>
                            Faktur Kerjasama 
                        </a>
                    </li>
                    <br><br><br><br>
                    <li class="text-logout">
                        <a href="<?= base_url; ?>/Logout" class="active">
                            <i class="material-icons-outlined">
                            logout
                            </i>
                            Logout 
                        </a>
                    </li>
                    
                </ul>
            </div>
        </div>