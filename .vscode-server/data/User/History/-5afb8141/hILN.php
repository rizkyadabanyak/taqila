<div class="page-sidebar">
            <div class="logo-box">
                <a href="#" id="sidebar-close">
                    <i class="material-icons">close</i>
                </a> 
            </div>
            <div class="page-sidebar-inner slimscroll">
                <ul class="accordion-menu">
                    <li class="sidebar-title">
                       Hi, <?= $_SESSION['name'] ?>
                    </li>
                    <li class="active-page active-title">
                        <a href="<?= base_url; ?>/Admin" class="active">
                            <i class="material-icons">bar_chart</i>
                            Statistik IDUKA
                        </a>
                    </li>
                    <li class="active-title">
                        <a href="<?= base_url; ?>/Admin" class="active">
                            <i class="material-icons-outlined">
                                work_outline
                            </i>
                            Kegiatan IDUKA
                        </a>
                    </li>
                    <li class="active-title">
                        <a href="<?= base_url; ?>/AdminDetailKegiatan" class="active">
                            <i class="material-icons-outlined">
                                work_history
                            </i>
                            Detail kegiatan 
                        </a>
                    </li>
                    <li class="active-title">
                        <a href="<?= base_url; ?>/AdminDetailKegiatan" class="active">
                            <i class="material-icons-outlined">
                                business_center
                            </i>
                            Kerjasama Prodi 
                        </a>
                    </li>
                    <li class="active-title">
                        <a href="./pages/kegiatan.html" class="active">
                            <i class="material-icons-outlined">folder</i> 
                            Master
                            <i class="material-icons has-sub-menu">add</i></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="404.html">404</a>
                            </li>
                            <li>
                                <a href="500.html">500</a>
                            </li>

                        </ul>
                    </li>
                    <li class="active-title">
                        <a href="<?= base_url; ?>/AdminDetailKegiatan" class="active">
                            <i class="material-icons-outlined">
                            description
                            </i>
                            Faktur Kerjasama 
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url; ?>/Logout" class="btn btn-danger">Logout</a>
                    </li>
                </ul>
            </div>
        </div>