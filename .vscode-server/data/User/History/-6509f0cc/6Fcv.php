<script>

  const labelsBar = [
    'Status Mitra',
  ];

  const dataBar = {
  labels: labelsBar,
  datasets: [
      {
    label: 'Active',
    data: [
        <?php 
           oci_execute($data['statusAktif']);
           while($p=oci_fetch_array($data['statusAktif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(70, 193, 211)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Passive',
    data: [
        <?php 
           oci_execute($data['statusPasif']);
           while($p=oci_fetch_array($data['statusPasif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
        'rgb(200, 84, 87)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Warning',
    data: [
        <?php 
           oci_execute($data['statusWarning']);
           while($p=oci_fetch_array($data['statusWarning'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
        'rgb(255, 207, 105)',
    ],
    hoverOffset: 4
  },
  {
    label: '2 Month Lagi',
    data: [
        <?php 
          oci_execute($data['statusMo']);
          while($p=oci_fetch_array($data['statusMo'])) 
           { 
              echo '"' . $p['COUNT(ID_STATUS)'] . '",';
           }
        ?>
    ],
    backgroundColor: [
        'rgb(131, 205, 171)',
    ],
    hoverOffset: 4
  },

]
};

    const configBar = {
        type: 'bar',
        data: dataBar,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

  const statusChart = new Chart(
    document.getElementById('statusChart'),
    configBar
  );

</script>

