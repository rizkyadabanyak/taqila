<script>

  const labelsBar = [
    'Active',
    'Passive',
    'Warning',
    '2 mo left',
  ];

  const dataBar = {
  labels: labelsBar,
  datasets: [
      {
    label: 'Active',
    data: [
        <?php 
           oci_execute($data['statusAktif']);
           while($p=oci_fetch_array($data['statusAktif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(255, 99, 132)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Passive',
    data: [
        <?php 
           oci_execute($data['statusPasif']);
           while($p=oci_fetch_array($data['statusAktif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(255, 99, 132)',
    ],
    hoverOffset: 4
  },

]
};

    const configBar = {
        type: 'bar',
        data: dataBar,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

  const statusChart = new Chart(
    document.getElementById('statusChart'),
    configBar
  );

</script>

