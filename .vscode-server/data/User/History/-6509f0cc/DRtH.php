<script>

  const labelsBar = [
    'Aktif',
    'Sebentar Lagi',
    'Pasif',
  ];

  const dataBar = {
  labels: labelsBar,
  datasets: [{
    label: 'lala',
    data: [
        <?php 
           oci_execute($data['status']);
           while($p=oci_fetch_array($data['status'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(165, 2, 100)'
    ],
    hoverOffset: 4
  }]
};

    const configBar = {
        type: 'bar',
        data: dataBar,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

  const statusChart = new Chart(
    document.getElementById('statusChart'),
    configBar
  );

</script>

