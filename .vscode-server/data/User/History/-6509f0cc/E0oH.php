<script>

  const labels = [
    'Aktif',
    'Sebentar Lagi',
    'Pasif',
  ];

  const data = {
  labels: labels,
  datasets: [{
    label: 'My First Dataset',
    data: [
        <?php 
           oci_execute($data['status']);
           while($p=oci_fetch_array($data['status'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)',
      'rgb(54, 162, 787)'
    ],
    hoverOffset: 4
  }]
};

    const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

  const statusChart = new Chart(
    document.getElementById('statusChart'),
    config
  );

</script>

