<script>

  const labelsBar = [
    'Status Mitra',
  ];

  const dataBar = {
  labels: labelsBar,
  datasets: [
      {
    label: 'Active',
    data: [
        <?php 
           oci_execute($data['statusAktif']);
           while($p=oci_fetch_array($data['statusAktif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
      'rgb(255, 99, 132)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Passive',
    data: [
        <?php 
           oci_execute($data['statusPasif']);
           while($p=oci_fetch_array($data['statusPasif'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
        'rgb(54, 162, 235)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Warning',
    data: [
        <?php 
           oci_execute($data['statusWarning']);
           while($p=oci_fetch_array($data['statusWarning'])) 
            { 
            echo '"' . $p['COUNT(ID_STATUS)'] . '",';
            }
        ?>
     ],
    backgroundColor: [
        'rgb(8, 22, 121)',
    ],
    hoverOffset: 4
  },
  {
    label: '2 Month Lagi',
    data: [
        <?php 
          oci_execute($data['statusMo']);
          while($p=oci_fetch_array($data['statusMo'])) 
           { 
               $hasilStatus = $p['COUNT(ID_STATUS)'];

           }
        ?>
        
        if (<?=$hasilStatus?>) == 0 {
            <?=$hasilStatus?>
        }

    ],
    backgroundColor: [
        'rgb(8, 22, 121)',
    ],
    hoverOffset: 4
  },

]
};

    const configBar = {
        type: 'bar',
        data: dataBar,
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        },
    };

  const statusChart = new Chart(
    document.getElementById('statusChart'),
    configBar
  );

</script>

