
            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active" aria-current="page">User Telegram</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                            <div class="col-xl-12 col-md-6 col-sm-6 ">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Tabel User Bot Telegram</h5>
                                        <div class="table-responsive">
                                        <table id="myTable" class="display table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>EMAIL USER</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                oci_execute($data['tele']);
                                                while($rows=oci_fetch_object($data['tele'])){
                                                ?>
                                                <tr>
                                                    <td><?php echo $rows->ID;?></td>
                                                    <td><?php echo $rows->EMAIL_USER;?></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>EMAIL USER</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>