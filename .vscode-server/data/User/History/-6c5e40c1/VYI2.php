<?php

class Admin extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
		
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "admin";
		$data['Name'] = $_SESSION['name'];
	    $this->view('views/partials-admin/header-gis',$data);
        $this->view('views/admin/statistikGis/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer');
		

	}
}