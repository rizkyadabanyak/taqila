<?php

class Admin extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
		
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "admin";
		$data['Name'] = $_SESSION['name'];

		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT M.NAMA_MITRA, M.LATITUDE, M.LONGITUDE, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, S.NAMA_STATUS, F.TGL_MITRA, F.TGL_BERAKHIR
		FROM FAKTUR F
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
		FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
		FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
		FULL OUTER JOIN STATUS_MITRA S ON  S.ID_STATUS = F.ID_STATUS";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['detail'] = $hasil;
		

		$sqlln = "SELECT COUNT(ID_AREA) FROM MITRA WHERE ID_AREA=1";
		$sqldn = "SELECT COUNT(ID_AREA) FROM MITRA WHERE ID_AREA=2";

		$sqlAktif ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=1";
		$sqlPasif ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=2";
		$sqlWarning ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=3";
		$sqlMo ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=4";

		$statusAktif = oci_parse($con,$sqlAktif);
		$statusPasif = oci_parse($con,$sqlPasif);
		$statusWarning = oci_parse($con,$sqlWarning);
		$statusMo = oci_parse($con,$sqlMo);
		$areaLn = oci_parse($con,$sqlln);
		$areaDn = oci_parse($con,$sqldn);


		oci_execute($statusAktif);
		oci_execute($statusPasif);
		oci_execute($statusWarning);
		oci_execute($statusMo);
		oci_execute($areaLn);
		oci_execute($areaDn);

		oci_fetch_all($statusAktif, $sa, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusPasif, $sp, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusWarning, $sw, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusMo, $sm, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($areaLn, $ln, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($areaDn, $dn, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

		$data['statusAktif'] = $statusAktif;
		$data['statusPasif'] = $statusPasif;
		$data['statusWarning'] = $statusWarning;
		$data['statusMo'] = $statusMo;
		$data['areaLn'] = $areaLn;
		$data['areaDn'] = $areaDn;

	
	    $this->view('views/partials-admin/header-gis',$data);
        $this->view('views/admin/statistikGis/v_index',$data);
		$this->view('views/admin/statistikGis/gis',$data);
		$this->view('views/admin/statistikGis/chart',$data);
		$this->view('views/admin/statistikGis/statusChart',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer');
		

	}
}