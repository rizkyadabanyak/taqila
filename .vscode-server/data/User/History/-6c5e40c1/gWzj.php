<?php

class Admin extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
		
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "admin";
		$data['Name'] = $_SESSION['name'];

		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT K.NAMA_KEGIATAN, KM.TIMESTAMP, M.NAMA_MITRA, M.LATITUDE, M.LONGITUDE, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, S.NAMA_STATUS, F.TGL_MITRA, F.TGL_BERAKHIR
		FROM FAKTUR F
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
		FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
		FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
		FULL OUTER JOIN STATUS_MITRA S ON  S.ID_STATUS = F.ID_STATUS
		LEFT OUTER JOIN KEGIATAN_MITRA KM ON KM.ID_MITRA = F.ID_MITRA
		LEFT OUTER JOIN KEGIATAN K ON K.ID_KEGIATAN = KM.ID_KEGIATAN";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);
		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);


		$sqlKegiatan ="SELECT K.NAMA_KEGIATAN, KM.TIMESTAMP
		FROM KEGIATAN_MITRA KM
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KM.ID_MITRA
		FULL OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
		WHERE M.ID_MITRA = 1";

		$hasilKegiatan = oci_parse($con, $sqlKegiatan);
		oci_execute($hasilKegiatan);
		oci_fetch_all($hasilKegiatan, $keg, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['detail'] = $hasil;
		$data['history'] = $hasilKegiatan;
		

		$sqll = "SELECT COUNT(ID_AREA) FROM MITRA GROUP BY ID_AREA";

		$area = oci_parse($con,$sqll);
		oci_execute($area);
		oci_fetch_all($area, $p, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		$data['area'] = $area;

		$sqlAktif ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=1";
		$sqlPasif ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=2";
		$sqlWarning ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=3";
		$sqlMo ="SELECT COUNT(ID_STATUS) FROM FAKTUR WHERE ID_STATUS=4";

		$statusAktif = oci_parse($con,$sqlAktif);
		$statusPasif = oci_parse($con,$sqlPasif);
		$statusWarning = oci_parse($con,$sqlWarning);
		$statusMo = oci_parse($con,$sqlMo);


		oci_execute($statusAktif);
		oci_execute($statusPasif);
		oci_execute($statusWarning);
		oci_execute($statusMo);

		oci_fetch_all($statusAktif, $sa, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusPasif, $sp, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusWarning, $sw, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		oci_fetch_all($statusMo, $sm, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);

		$data['statusAktif'] = $statusAktif;
		$data['statusPasif'] = $statusPasif;
		$data['statusWarning'] = $statusWarning;
		$data['statusMo'] = $statusMo;

	
	    $this->view('views/partials-admin/header-gis',$data);
        $this->view('views/admin/statistikGis/v_index',$data);
		$this->view('views/admin/statistikGis/gis',$data);
		$this->view('views/admin/statistikGis/chart',$data);
		$this->view('views/admin/statistikGis/statusChart',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer');
		

	}
}