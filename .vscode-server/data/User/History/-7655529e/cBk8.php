
            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Kerjasama Prodi</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds many advanced features to any HTML table.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-xl-12 col-md-6 col-sm-6 ">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Zero Configuration</h5>
                                        <div class="table-responsive">
                                        <table id="myTable" class="display table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Jurusan</th>
                                                    <th>Mitra</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                oci_execute($data['prodi']);
                                                while($rows=oci_fetch_object($data['prodi'])){
                                                ?>
                                                <tr>
                                                    <td><?php echo $rows->ID_KERJASAMA_PRODI;?></td>
                                                    <td><?php echo $rows->JURUSAN;?></td>
                                                    <td><?php echo $rows->NAMA_MITRA;?></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Jurusan</th>
                                                    <th>Mitra</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>