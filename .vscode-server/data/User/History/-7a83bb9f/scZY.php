<?php

class AdminBackup extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
        $data['title'] = 'SIMIDUKA';
		$data['Name'] = $_SESSION['name'];
        $this->view('views/admin/partials/header',$data);
        $this->view('views/admin/statistikGis/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer');

    }
}