
            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Area</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc">Area Mitra PENS</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-xl-12 col-md-6 col-sm-6 ">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Tabel Area mitra</h5>
                                        <div class="table-responsive">
                                        <table id="myTable" class="display table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>AREA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                oci_execute($data['detail']);
                                                while($rows=oci_fetch_object($data['detail'])){
                                                ?>
                                                <tr>
                                                    <td><?php echo $rows->ID_MITRA;?></td>
                                                    <td><?php echo $rows->NAMA_AREA;?></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>AREA</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>