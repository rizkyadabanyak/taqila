<script>

  const labels = [
    'Luar Negeri',
    'Dalam Negeri',
  ];

  const data = {
  labels: labels,
  datasets: [{
    label: 'Luar Negeri','Dalam Negeri'
    data: [
        <?php 
           oci_execute($data['area']);
           while($p=oci_fetch_array($data['area'])) 
            { 
            echo '"' . $p['COUNT(ID_AREA)'] . '",';
            }
        ?>
       
    ],
    backgroundColor: [
      'rgb(255, 99, 132)',
      'rgb(54, 162, 235)'
    ],
    hoverOffset: 4
  }]
};

    const config = {
        type: 'doughnut',
        data: data,
    };

  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

</script>

