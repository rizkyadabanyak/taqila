<script>

  const labels = [
    'Luar Negeri',
    'Dalam Negeri',
  ];

  const data = {
  labels: labels,
  datasets: [{
    label: 'Luar Negeri',
    data: [
        <?php 
           oci_execute($data['areaLn']);
           while($p=oci_fetch_array($data['areaLn'])) 
            { 
            echo '"' . $p['COUNT(ID_AREA)'] . '",';
            }
        ?>
       
    ],
    backgroundColor: [
      'rgb(255, 99, 132)',
    ],
    hoverOffset: 4
  },
  {
    label: 'Dalam Negeri',
    data: [
        <?php 
           oci_execute($data['areaDn']);
           while($p=oci_fetch_array($data['areaDn'])) 
            { 
            echo '"' . $p['COUNT(ID_AREA)'] . '",';
            }
        ?>
       
    ],
    backgroundColor: [
      'rgb(54, 162, 235)',
    ],
    hoverOffset: 4
  }
]
};

    const config = {
        type: 'doughnut',
        data: data,
    };

  const myChart = new Chart(
    document.getElementById('myChart'),
    config
  );

</script>

