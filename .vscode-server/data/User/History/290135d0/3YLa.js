$(document).ready(function() {
        
    "use strict";
    $('#zero-conf').DataTable({
        buttons: [
             'excel', 'pdf'
        ],
        paging: true,
        responsive: true,
        dom: 
        "<'row'<'col-md-7'B><'col-md-5'f>>" +
        "<'row'<'col-md-12'tr>>" +
        "<'row'<'col-md-5'i><'col-md-7'p>>",
    });
});
