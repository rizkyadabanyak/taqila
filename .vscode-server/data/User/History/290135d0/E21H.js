$(document).ready(function() {

    var data = [
        {'status':'Active'},
        {'status':'Passive'},
        {'status':'2 month left'},
    ]
    
    "use strict";
    $('#zero-conf').DataTable({
        data : data,
        columns:[
            data:'status', name:'status',
            render: function(data,type,row){
                console.log('Content of data is : '+data);
		  sev='';
            switch (data){
                case 'Active':
                    sev = '<span class="badge badge-danger badge-pill">'+data+'</span>';
                    break;
                case 'Passive':
                    sev = '<span class="badge badge-warning badge-pill">'+data+'</span>';
                    break;
                case '2 month left':
                    sev = '<span class="badge bg-yellow badge-pill">'+data+'</span>';
                    break;
            }
            }
        ],
        buttons: [
             'excel', 'pdf'
        ],
        dom: 
        "<'row'<'col-md-7'B><'col-md-5'f>>" +
        "<'row'<'col-md-12'tr>>" +
        "<'row'<'col-md-5'i><'col-md-7'p>>",
    });

    $('#complex-header').DataTable();

    var groupColumn = 2;
    var table = $('#row-grouping').DataTable({
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "order": [[ groupColumn, 'asc' ]],
        "displayLength": 25,
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }
    } );
 
    // Order by the grouping
    $('#row-grouping tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
    
    var t = $('#add-row').DataTable();
    var counter = 1;
 
    $('#addRow').on( 'click', function (e) {
        t.row.add( [
            counter +'.1',
            counter +'.2',
            counter +'.3',
            counter +'.4',
            counter +'.5'
        ] ).draw( false );
        counter++;
        e.preventDefault();
    } );
 
    // Automatically add a first row of data
    $('#addRow').click();

    
});
