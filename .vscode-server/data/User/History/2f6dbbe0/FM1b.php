
            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Faktur Kerjasama</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, built upon the foundations of progressive enhancement, that adds many advanced features to any HTML table.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-xl-12 col-md-6 col-sm-6 ">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">Zero Configuration</h5>
                                        <table id="zero-conf" class="display" style="width:80%">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>MITRA</th>
                                                    <th>STATUS</th>
                                                    <th>JUDUL</th>
                                                    <th>JENIS</th>
                                                    <th>PIC MITRA</th>
                                                    <th>PIC PENS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                oci_execute($data['detail']);
                                                while($rows=oci_fetch_object($data['detail'])){
                                                ?>
                                                <tr>
                                                    <td><?php echo $rows->ID_FAKTUR;?></td>
                                                    <td><?php echo $rows->NAMA_MITRA;?></td>
                                                    <td><?php echo $rows->NAMA_STATUS;?></td>
                                                    <td><?php echo $rows->JUDUL_KERJASAMA;?></td>
                                                    <td><?php echo $rows->NAMA_JENIS;?></td>
                                                    <td><?php echo $rows->PIC_MITRA;?></td>
                                                    <td><?php echo $rows->PIC_PENS;?></td>

                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>MITRA</th>
                                                    <th>STATUS</th>
                                                    <th>JUDUL</th>
                                                    <th>JENIS</th>
                                                    <th>PIC MITRA</th>
                                                    <th>PIC PENS</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>