<?php

class AdminKerjasamaProdi extends Controller {
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "prodi";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT KP.ID_KERJASAMA_PRODI, KP.JURUSAN, M.NAMA_MITRA FROM KERJASAMA_PRODI KP FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KP.ID_MITRA";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['prodi'] = $hasil;

	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/admin/prodi/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer-detailKegiatan');
	}
}