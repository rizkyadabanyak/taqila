
            <div class="login-container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="auth-form">
                            <div class="row">
                                <div class="col">
                                <div class="d-flex justify-content-center">
                                    <div class="logo-box"><a href="#" class="heading-text1">Login <?= $data['cek']; ?></a></div>
                                </div>
                                    <form action="<?= base_url; ?>/LoginAdmin/prosesLogin" method="POST">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                            <i class="material-icons">visibility_off</i>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block btn-submit">Sign In</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2"> 
                    </div>
                    <div class="col-lg-6">
                        <div class="d-flex justify-content-center">
                            <img src="<?= base_url; ?>/assets/images/maps_5.svg" alt="" class="auth-img"/>
                        </div>  
                    </div>
                </div>
            </div>
            <div>