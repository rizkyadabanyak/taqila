
            <div class="login-container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="auth-form">
                            <div class="row">
                                <div class="col">
                                    <div class="logo-box"><a href="#" class="logo-text">Login page - <?= $data['cek']; ?></a></div>
                                    <form action="<?= base_url; ?>/LoginAdmin/prosesLogin" method="POST">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block btn-submit">Sign In</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 d-none d-lg-block d-xl-block">
                    <img src="<?= base_url; ?>/assets/images/maps_5.svg" alt=""class=""/>  
                    </div>
                </div>
            </div>
            <div>