<?php

class AdminKegiatan extends Controller {
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "kegiatan";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT KM.ID_KEGIATAN_MITRA, M.NAMA_MITRA, K.NAMA_KEGIATAN, KM.TIMESTAMP
		FROM KEGIATAN_MITRA KM
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KM.ID_MITRA
		FULL OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
		WHERE K.NAMA_KEGIATAN IS NOT NULL AND M.NAMA_MITRA IS NOT NULL
		";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['detail'] = $hasil;

	    $this->view('views/partials-admin/header',$data);
	    $this->view('views/admin/kegiatan/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer-detailKegiatan');
	}
}