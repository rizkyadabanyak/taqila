            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">GIS & Statistik</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc"><b>Geographic Information System</b> - Peta persebaran kemitraan PENS</p>
                            </div>
                        </div>
                    </div>

                        <div class="row">
                            <div class="col">
                                <div id="mapid" style="width: 100%; height: 400px;"></div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Status mitra PENS</h5>
                                    <canvas id="statusChart"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Area mitra PENS</h5>
                                    <canvas id="myChart"></canvas>
                                </div>
                            </div>
                        </div>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
 