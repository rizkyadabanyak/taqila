            <div class="page-content">
                <div class="page-info container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">IDUKA</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Statistik</li>
                        </ol>
                    </nav>
                </div>
                <div class="main-wrapper container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-title">
                                <p class="page-desc"><b>Web GIS</b> is an open-source modern charting library that helps developers to create beautiful and interactive visualizations for web pages.</p>
                            </div>
                        </div>
                    </div>

                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-body">
                                    <div id="mapid" style="width: 100%; height: 200px;"></div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Line Chart</h5>
                                    <div id="apex1"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Area Chart</h5>
                                    <div id="apex2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

    var map = L.map('mapid').setView([-7.264034926981125, 112.72366678193039], 2);

    var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(map);

    var markers = L.markerClusterGroup();

    <?php
        $db_user = "PA0009";
        $db_pass = "806307";
        $con = konekDb($db_user,$db_pass);

        $sql = "SELECT * FROM MITRA";

        $hasil = oci_parse($con, $sql);
        oci_execute($hasil);
    
        while($r2= oci_fetch_array($hasil, OCI_BOTH)){
            $id = $r2['ID_MITRA'];
            $name = $r2['NAMA_MITRA'];
            $lat = $r2['LATITUDE'];
            $lng = $r2['LONGITUDE']
        ?>

    var marker = L.marker([<?=$lat?>, <?=$lng?>]).bindPopup("id : <?= $id?> <br/> nama : <?=$nama?>");
    markers.addLayer(marker);
    <?php
    }
    ?>
    map.addLayer(markers);

</script>