<?php

class AdminArea extends Controller {
	
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
        $data['title'] = 'SIMIDUKA';
		$data['Name'] = $_SESSION['name'];
        $this->view('views/partials-admin/header1',$data);
	    $this->view('views/admin/master/index_area',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer1')

    }
}