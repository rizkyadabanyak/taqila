<?php

class AdminFakturKerjasama extends Controller {
	public function __construct()
	{
		$this->level();
	}

	public function index()
	{
	    $data['title'] = 'SIMIDUKA';
	    $data['halaman']  = "faktur";
		$data['Name'] = $_SESSION['name'];


		$db_user = "PA0009";
		$db_pass = "806307";
		$con = konekDb($db_user,$db_pass);

		$sql = "SELECT F.ID_FAKTUR, M.NAMA_MITRA, F.JUDUL_KERJASAMA, J.NAMA_JENIS, M.ALAMAT_MITRA, M.PIC_MITRA, F.PIC_PENS, S.NAMA_STATUS, F.TGL_MITRA, F.TGL_BERAKHIR
		FROM FAKTUR F
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = F.ID_MITRA
		FULL OUTER JOIN JENIS_KERJASAMA J ON  J.ID_JENIS = F.ID_JENIS
		FULL OUTER JOIN BENTUK_KERJASAMA B ON  B.ID_BENTUK = F.ID_BENTUK
		FULL OUTER JOIN STATUS_MITRA S ON  S.ID_STATUS = F.ID_STATUS";

		$hasil = oci_parse($con, $sql);
		oci_execute($hasil);

		oci_fetch_all($hasil, $rows, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
			
		// foreach ($rows as $hasil) {
		// 		echo "Nama : " . $hasil['JUDUL'] ." - Stok : " . $hasil['DSC'] . "<br>";
		// 	}
		$data['detail'] = $hasil;

		$date_now = date('d M y');
		$date_sixMounthAgo = date('d M y',strtotime("-180 day",strtotime(date('d M y'))));
		$sqlnotifikasi = "SELECT KM.ID_KEGIATAN_MITRA, M.NAMA_MITRA, K.NAMA_KEGIATAN, KM.TIMESTAMP
		FROM KEGIATAN_MITRA KM
		FULL OUTER JOIN MITRA M ON  M.ID_MITRA = KM.ID_MITRA
		FULL OUTER JOIN KEGIATAN K ON  K.ID_KEGIATAN = KM.ID_KEGIATAN
		WHERE KM.TIMESTAMP NOT BETWEEN '$date_sixMounthAgo' AND '$date_now'";


		$notifikasi = oci_parse($con,$sqlnotifikasi);
		oci_execute($notifikasi);
		oci_fetch_all($notifikasi, $p, 0, 0, OCI_FETCHSTATEMENT_BY_ROW);
		$data['notifikasi'] = $notifikasi;

		

	    $this->view('views/partials-admin/header1',$data);
	    $this->view('views/admin/fakturKerjasama/v_index',$data);
        $this->view('views/partials-admin/sidebar',$data);
	    $this->view('views/partials-admin/footer1');
	}
}