<script>

var map = L.map('mapid').setView([23.249406512898343, 67.25277836856507], 1);

var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);

var markers = L.markerClusterGroup();

<?php
    
        oci_execute($data['detail']);
        oci_execute($data['history']);

            $nama = $rows['NAMA_MITRA'];
            $judul = $rows['JUDUL_KERJASAMA'];
            $jenis = $rows['NAMA_JENIS'];
            $alamat = $rows['ALAMAT_MITRA'];
            $pic = $rows['PIC_MITRA'];
            $status = $rows['NAMA_STATUS'];
            $tgl_berakhir = $rows['TGL_BERAKHIR'];
            $lat = $rows['LATITUDE'];
            $lng = $rows['LONGITUDE'];

            $namakegiatan = $rows['NAMA_KEGIATAN'];
            $timestamp = $rows['TIMESTAMP'];

        while($rows=oci_fetch_array($data['detail'])){
            $nama = $rows['NAMA_MITRA'];
            $judul = $rows['JUDUL_KERJASAMA'];
            $jenis = $rows['NAMA_JENIS'];
            $alamat = $rows['ALAMAT_MITRA'];
            $pic = $rows['PIC_MITRA'];
            $status = $rows['NAMA_STATUS'];
            $tgl_berakhir = $rows['TGL_BERAKHIR'];
            $lat = $rows['LATITUDE'];
            $lng = $rows['LONGITUDE'];

            $namakegiatan = $rows['NAMA_KEGIATAN'];
            $timestamp = $rows['TIMESTAMP'];
           

            if ($rows['NAMA_STATUS'] == "Active"){
                $namakegiatan = $rows['NAMA_KEGIATAN'];
            $timestamp = $rows['TIMESTAMP'];= "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_25.svg";
            }elseif($rows['NAMA_STATUS'] == "Passive"){
                $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_28.svg";
            }elseif($rows['NAMA_STATUS'] == "2 month left"){
                $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_27.svg";
            }
            ?>

                    var marker = L.marker([<?=$lat?>, <?=$lng?>]).bindPopup(
                        "<div class=`row`> <div class=`col-md-6`> <img src='<?=$src?>' /> <br/> <br/><b>Detail Informasi <?=$nama?> </b> <br/> <br/> <b> Nama mitra :</b> <?=$nama?> <br/> <b>Judul mitra : </b> <?=$judul?> <br/> <b> Jenis kerjasama : </b> <?=$jenis?><br/> <b> Alamat mitra : </b><?=$alamat?> <br/><b> PIC mitra : </b><?=$pic?> <br/></div> <div class=`col-md-6`><b> Status : </b><?=$status?> hingga <?=$tgl_berakhir?> <hr/> <b>Riwayat Kegiatan : </b></br> <?=$namakegiatan?> - <?=$timestamp?></div></div>");
                    markers.addLayer(marker);
                    <?php
        }
        ?>
        map.addLayer(markers);

</script>



<?php
    
        oci_execute($data['detail']);
        oci_execute($data['history']);

            $nama =" ";
            $judul = " ";
            $jenis = " ";
            $alamat = " ";
            $pic = " ";
            $status = " ";
            $tgl_berakhir = " ";
            $lat = " ";
            $lng = " ";
            $namakegiatan = " ";
            $timestamp =" ";
            $src =" ";
            $kegiatanPerTitik = array();

            while ($rows=oci_fetch_array($data['detail'])) {
                if(array_key_exists($rows["NAMA_MITRA"]), $kegiatanPerTitik) {
                    $kegiatanPerTitik[$rows["NAMA_MITRA"]] = array();
                }

                $kegiatanPerTitik[$rows["NAMA_MITRA"]] = array_push($kegiatanPerTitik[$rows["NAMA_MITRA"]], { 
                    $nama = $rows['NAMA_MITRA'];
                    $judul = $rows['JUDUL_KERJASAMA'];
                    $jenis = $rows['NAMA_JENIS'];
                    $alamat = $rows['ALAMAT_MITRA'];
                    $pic = $rows['PIC_MITRA'];
                    $status = $rows['NAMA_STATUS'];
                    $tgl_berakhir = $rows['TGL_BERAKHIR'];
                    $lat = $rows['LATITUDE'];
                    $lng = $rows['LONGITUDE'];
                    $namakegiatan = $rows['NAMA_KEGIATAN'];
                    $timestamp = $rows['TIMESTAMP'];

                    if ($rows['NAMA_STATUS'] == "Active"){
                        $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_28.svg";
                    }elseif($rows['NAMA_STATUS'] == "Passive"){
                        $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_27.svg";
                    }elseif($rows['NAMA_STATUS'] == "2 month left"){
                        $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_25.svg";
                    }
                });

              
            }

            foreach ($kegiatanPerTitik as $titik)
            {
                ?>
                var marker = L.marker([<?=$titik->$lat?>, <?=$titik->$lng?>]).bindPopup(
                        "<div class=`row`> <div class=`col-md-6`> <img src='<?=$titik->$src?>' /> <br/> <br/><b>Detail Informasi <?=$titik->$nama?> </b> <br/> <br/> <b> Nama mitra :</b> <?=$titik->$nama?> <br/> <b>Judul mitra : </b> <?=$titik->$judul?> <br/> <b> Jenis kerjasama : </b> <?=$titik->$jenis?><br/> <b> Alamat mitra : </b><?=$titik->$alamat?> <br/><b> PIC mitra : </b><?=$titik->$pic?> <br/></div> <div class=`col-md-6`><b> Status : </b><?=$titik->$status?> hingga <?=$titik->$tgl_berakhir?> <hr/> <b>Riwayat Kegiatan : </b></br> <?=$titik->$namakegiatan?> - <?=$titik->$timestamp?></div></div>");
                    markers.addLayer(marker);
                <?php
            }
            ?>
        /*
        while($rows=oci_fetch_array($data['detail'])){
            $nama = $rows['NAMA_MITRA'];
            $judul = $rows['JUDUL_KERJASAMA'];
            $jenis = $rows['NAMA_JENIS'];
            $alamat = $rows['ALAMAT_MITRA'];
            $pic = $rows['PIC_MITRA'];
            $status = $rows['NAMA_STATUS'];
            $tgl_berakhir = $rows['TGL_BERAKHIR'];
            $lat = $rows['LATITUDE'];
            $lng = $rows['LONGITUDE'];
            $namakegiatan = $rows['NAMA_KEGIATAN'];
            $timestamp = $rows['TIMESTAMP'];
           

            if ($rows['NAMA_STATUS'] == "Active"){
                $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_28.svg";
            }elseif($rows['NAMA_STATUS'] == "Passive"){
                $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_27.svg";
            }elseif($rows['NAMA_STATUS'] == "2 month left"){
                $src = "https://project.mis.pens.ac.id/mis114/contents/assets/images/Group_25.svg";
            ?>
        */
        map.addLayer(markers);

<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.bootstrap4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<link href="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js" rel="stylesheet">

<li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons-outlined">notifications</i><span class="badge badge-pill badge-danger float-right">21</span></a>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Calendar</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Settings &amp Privacy</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Switch Account</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Log out</a>
                                </div>
                            </li>